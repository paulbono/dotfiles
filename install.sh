for file in dotfiles/*
do
    filename=$(basename $file)
    echo "Copying $file to $HOME/.$filename"
    cp -f "$file" "$HOME/.$filename"
done

